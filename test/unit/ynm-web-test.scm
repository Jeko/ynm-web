(define-module (test unit harness)
  #:use-module (spec)
  #:use-module (ice-9 receive)
  #:use-module (web client)
  #:use-module (json))

(install-spec-runner-repl)

(define-public run-tests
  (lambda ()
    (start-server)
    (describe "/ingredients"
      (it "should retrieve all ingredients in base"
	(should= 2109 (vector-length (cdr (assoc "results" (json-string->scm (response-body "http://127.0.0.1:3000/ingredients")))))))
      (stop-server))))

(define (start-server)
  (system "art work &" )
  (wait-for-server-to-be-ready))

(define (stop-server)
  (system* "killall" ".art-real"))

(define (wait-for-server-to-be-ready)
  (let ([ONE_SECOND 1])
    (wait-for ONE_SECOND)))

(define (wait-for seconds)
  (let wait ([time-ref (current-time)])
    (if (< (- (current-time) time-ref) seconds)
	(wait time-ref))))

(define (response-body URI)
  (receive (response body)
      (http-request URI)
    body))

;; /ingredients
;;;; DONE should have attribute count : the number of ingredient
;;;; should have attribute next : the next page of results
;;;; should have attribute previous : the previous page of results
;;;; should have attribute result : the ingredient ressources

;; /ingredients/:id
;;;; should have attribute alim_code
;;;; should have attribute food_label
;;;; should have attribute hypoth
;;;; should have attribute intake
;;;;;;; should have attribute nrj_kj
;;;;;;; should have attribute nrj_kcal
;;;; should have attribute url : the hypermedia URL of this resource.
